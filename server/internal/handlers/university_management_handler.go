package handlers

import (
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"log"
	"time"
	"university-management-golang/db/connection"
	um "university-management-golang/protoclient/university_management"
)

type universityManagementServer struct {
	um.UniversityManagementServiceServer
	connectionManager connection.DatabaseConnectionManager
}

func (u *universityManagementServer) GetDepartment(ctx context.Context, request *um.GetDepartmentRequest) (*um.GetDepartmentResponse, error) {
	connection, err := u.connectionManager.GetConnection()
	defer u.connectionManager.CloseConnection()

	if err != nil {
		log.Fatalf("Error: %+v", err)
	}

	var department um.Department
	connection.GetSession().Select("id", "name").From("department").Where("id = ?", request.GetId()).LoadOne(&department)

	_, err = json.Marshal(&department)
	if err != nil {
		log.Fatalf("Error while marshaling %+v", err)
	}

	return &um.GetDepartmentResponse{Department: &um.Department{
		Id:   department.Id,
		Name: department.Name,
	}}, nil
}

func (u *universityManagementServer) GetStudentRecord(ctx context.Context, request *um.GetStudentRequest) (*um.GetStudentResponse, error) {
	connection, err := u.connectionManager.GetConnection()
	defer u.connectionManager.CloseConnection()

	if err != nil {
		log.Fatalf("Error: %+v", err)
	}

	var student []*um.Student
	connection.GetSession().Select("rollno", "name", "deptid").From("student").Where("deptid = ?", request.GetDeptId()).LoadOne(&student)

	_, err = json.Marshal(&student)
	if err != nil {
		log.Fatalf("Error while marshaling %+v", err)
	}

	return &um.GetStudentResponse{Student: student}, nil
}

func (u *universityManagementServer) UpdateStudentRecord(ctx context.Context, request *um.UpdateStudentRequest) (*um.Empty, error) {
	connection, err := u.connectionManager.GetConnection()
	defer u.connectionManager.CloseConnection()

	if err != nil {
		log.Fatalf("Error: %+v", err)
	}

	log.Println("Inside Handler")
	log.Println(request.GetStudent())
	result, err := connection.GetSession().InsertInto("student").Columns("rollno", "name", "deptid").Values(request.Student.GetRollno(), request.Student.GetName(), request.Student.GetDeptid()).Exec()
	log.Println(result)
	if err != nil {
		log.Println("Error: %+v", err)
	}
	log.Println("Inside Handler next to sql query")
	return &um.Empty{}, nil
}

func (u *universityManagementServer) GetStaffRecordForGivenStudent(ctx context.Context, request *um.GetStaffRecordRequest) (*um.GetStaffRecordResponse, error) {
	connection, err := u.connectionManager.GetConnection()
	defer u.connectionManager.CloseConnection()

	if err != nil {
		log.Fatalf("Error: %+v", err)
	}

	var staffRecords []*um.StaffRecord
	var deptId int32
	connection.GetSession().Select("deptid").From("student").Where("rollno = ?", request.GetRollno()).Load(&deptId)
	connection.GetSession().Select("empid", "name", "deptid").From("staffrecords").Where("deptid = ?", deptId).Load(&staffRecords)

	_, err = json.Marshal(&staffRecords)
	if err != nil {
		log.Fatalf("Error while marshaling %+v", err)
	}

	return &um.GetStaffRecordResponse{StaffRecord: staffRecords}, nil
}

func (u *universityManagementServer) StudentLogin(ctx context.Context, request *um.StudentLoginRequest) (*um.Empty, error) {
	connection, err := u.connectionManager.GetConnection()
	defer u.connectionManager.CloseConnection()

	if err != nil {
		log.Fatalf("Error: %+v", err)
	}

	var student *um.Student
	_, err = connection.GetSession().Select("*").From("student").Where("rollno = ?", request.Rollno).Load(&student)
	if err != nil {
		log.Fatalf("Error: %+v", err)
	}
	if student == nil {
		log.Println("Invalid roll number")
		return &um.Empty{}, errors.New("Invalid roll number")
	}
	if student.Loginstatus == true {
		log.Println(student.Name + " is not logged out yet")
		return &um.Empty{}, errors.New(student.Name + " is not logged out yet")
	}
	_, err = connection.GetSession().InsertInto("attendance").Columns("rollno", "login").Values(request.Rollno, time.Now()).Exec()
	if err != nil {
		log.Println(err)
	}
	_, err = connection.GetSession().Update("student").Set("loginstatus", true).Where("rollno = ?", student.Rollno).Exec()
	if err != nil {
		log.Println(err)
	}

	return &um.Empty{}, nil
}

func (u *universityManagementServer) StudentLogout(ctx context.Context, request *um.StudentLogoutRequest) (*um.Empty, error) {
	connection, err := u.connectionManager.GetConnection()
	defer u.connectionManager.CloseConnection()

	if err != nil {
		log.Fatalf("Error: %+v", err)
	}

	var student *um.Student
	_, err = connection.GetSession().Select("*").From("student").Where("rollno = ?", request.Rollno).Load(&student)
	if err != nil {
		log.Fatalf("Error: %+v", err)
	}
	if student == nil {
		log.Println("Invalid roll number")
		return &um.Empty{}, errors.New("Invalid roll number")
	}
	if student.Loginstatus == false {
		log.Println(student.Name + " is not yet logged in")
		return &um.Empty{}, errors.New(student.Name + " is not yet logged in")
	}
	_, err = connection.GetSession().Update("attendance").Set("logout", time.Now()).Where("rollno = ? and logout is null", student.Rollno).Exec()
	if err != nil {
		log.Println(err)
	}
	_, err = connection.GetSession().Update("student").Set("loginstatus", false).Where("rollno = ?", student.Rollno).Exec()
	if err != nil {
		log.Println(err)
	}

	return &um.Empty{}, nil
}


func NewUniversityManagementHandler(connectionmanager connection.DatabaseConnectionManager) um.UniversityManagementServiceServer {
	return &universityManagementServer{
		connectionManager: connectionmanager,
	}
}
