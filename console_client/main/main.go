package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"university-management-golang/protoclient/university_management"
)

const (
	host = "localhost"
	port = "2345"
)

func main() {
	conn, err := grpc.Dial(fmt.Sprintf("%s:%s", host, port), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error occured %+v", err)
	}
	client := university_management.NewUniversityManagementServiceClient(conn)
	//var departmentID int32 = 1
	//departmentResponse, err := client.GetDepartment(context.TODO(), &university_management.GetDepartmentRequest{Id: departmentID})
	//if err != nil {
	//	log.Fatalf("Error occured while fetching department for id %d,err: %+v", departmentID, err)
	//}
	//log.Println(departmentResponse)
	//studentRecord, err := client.GetStudentRecord(context.TODO(), &university_management.GetStudentRecordRequest{DeptId: departmentID})
	//if err != nil {
	//	log.Fatalf("Error occured while fetching student record for id %d,err: %+v", departmentID, err)
	//}
	//var studentRecord = &university_management.StudentRecord{
	//	RollNo: 4,
	//	Name: "Rohan",
	//	DeptId: 4,
	//}
	//_, err = client.UpdateStudentRecord(context.TODO(), &university_management.UpdateStudentRecordRequest{StudentRecord: studentRecord})
	//if err != nil {
	//	log.Fatalf("Error occured while updating student record for rollno %d,err: %+v", studentRecord.RollNo, err)
	//}
	//log.Println(studentRecord)

	//staff, err := client.GetStaffRecordForGivenStudent(context.TODO(), &university_management.GetStaffRecordRequest{Rollno: 2})
	//if err != nil {
	//	log.Fatalf("Error occured while retrieving student record for rollno %d,err: %+v", 2, err)
	//}
	//log.Println(staff)

	//rollNo := int32(1)
	//_, err = client.StudentLogin(context.TODO(), &university_management.StudentLoginRequest{Rollno: rollNo})
	//if err != nil {
	//	log.Fatalf("Error occured while logining in attendance of a student with rollno %d,err: %+v", rollNo, err)
	//}

	rollNo := int32(1)
	_, err = client.StudentLogout(context.TODO(), &university_management.StudentLogoutRequest{Rollno: rollNo})
	if err != nil {
		log.Fatalf("Error occured while logining in attendance of a student with rollno %d,err: %+v", rollNo, err)
	}
}
