CREATE TABLE IF NOT EXISTS studentRecords (
    rollNo bigint NOT NULL,
    name text,
    deptID bigint
);