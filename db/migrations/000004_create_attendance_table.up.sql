CREATE TABLE IF NOT EXISTS attendance (
    rollno bigint NOT NULL,
    login TIMESTAMP,
    logout TIMESTAMP
);