CREATE TABLE IF NOT EXISTS staffRecords (
    empId bigint NOT NULL,
    name text,
    deptID bigint
);