module university-management-golang

go 1.16

require (
	github.com/gocraft/dbr/v2 v2.7.2
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/pkg/errors v0.9.1
	github.com/shuLhan/go-bindata v4.0.0+incompatible // indirect
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)
